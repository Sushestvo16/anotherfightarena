import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (!fighter) return '';
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const { source, name, health, attack, defense } = fighter;
  const attributes = {
    source,
    name,
    health,
    attack,
    defense
  };
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighterElement.innerHTML = `<div class="title__preview">${name}</div>
  <img src=${source} />
  <div class="preview__properties">
  <span >HEALTH:${health}</span>
  </div >
  <div class="preview__properties">
  <span>ATTACK:${attack}</span>
  </div>
  <div class="preview__properties">
  <span>DEFENSE:${defense}</span>
  </div>
  `


  // todo: show fighter info (image, name, health, etc.)



  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });


  return imgElement;
}
