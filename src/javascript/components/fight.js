  import { controls } from '../../constants/controls';
import { showWinnerModal } from '../components/modal/winner'
export async function fight(firstFighter, secondFighter) {
  let leftFighter = [], rightFighter = [];
  for (const [key, value] of Object.entries(firstFighter)) {
    leftFighter[key] = firstFighter[key];
  }
  for (const [key, value] of Object.entries(secondFighter)) {
    rightFighter[key] = secondFighter[key];
  }
  leftFighter.healthIndicator = document.getElementById('left-fighter-indicator');
  rightFighter.healthIndicator = document.getElementById('right-fighter-indicator');
  leftFighter.dynamicHealth = leftFighter.health;
  rightFighter.dynamicHealth = rightFighter.health;
  leftFighter.healthIndicator.innerHTML = `${leftFighter.dynamicHealth}hp`;
  rightFighter.healthIndicator.innerHTML = `${rightFighter.dynamicHealth}hp`;
  leftFighter.combo = [];
  rightFighter.combo = [];
    leftFighter.allowCombo = true;
  rightFighter.allowCombo = true;
  leftFighter.leftFighterWin = false;
  rightFighter.rightFighterWin = false;

  document.addEventListener('keydown', (event) => {
    let attacker, defender;
    const keyName = event.code;

    for (const [key, value] of Object.entries(controls)) {
      attacker = defender = false;
      leftFighter.isCombo = false;
      rightFighter.isCombo = false;

      if (Array.isArray(value)) {
        if (key == 'PlayerOneCriticalHitCombination' && controls['PlayerOneCriticalHitCombination'].includes(keyName)) leftFighter.combo.push(keyName);
        if (key == 'PlayerTwoCriticalHitCombination' && controls['PlayerTwoCriticalHitCombination'].includes(keyName)) rightFighter.combo.push(keyName);

        leftFighter.isCombo = JSON.stringify(leftFighter.combo.sort()) == JSON.stringify(value.sort()) && leftFighter.allowCombo;

        rightFighter.isCombo = JSON.stringify(rightFighter.combo.sort()) == JSON.stringify(value.sort()) && rightFighter.allowCombo;

      }

      if (keyName == value || leftFighter.isCombo || rightFighter.isCombo) {
        let damage = 0;

        console.log({
          'before': 'before',
          'first health': leftFighter.dynamicHealth,
          'second health': rightFighter.dynamicHealth,
        });

        //Set position
        switch (key) {
          case 'PlayerOneAttack':
            if (leftFighter.isDefending) break;
            leftFighter.isAttacking = true;
            attacker = leftFighter;
            defender = rightFighter;
            break;

          case 'PlayerOneBlock':
            if (leftFighter.isAttacking) break;
            if (rightFighter.isCombo) break;
            leftFighter.isDefending = true;
            break;

          case 'PlayerTwoAttack':
            if (rightFighter.isDefending) break;
            rightFighter.isAttacking = true;
            attacker = rightFighter;
            defender = leftFighter;
            break;

          case 'PlayerTwoBlock':
            if (rightFighter.isAttacking) break;
            if (leftFighter.isCombo) break;
            rightFighter.isDefending = true;
            break;

          case 'PlayerOneCriticalHitCombination':
            if (!leftFighter.isCombo) break;
            attacker = leftFighter;
            defender = rightFighter;
            leftFighter.allowCombo = false;
            setTimeout(() => {
              leftFighter.allowCombo = true;
            }, 10000)
            break;

          case 'PlayerTwoCriticalHitCombination':
            if (!rightFighter.isCombo) break;
            attacker = rightFighter;
            defender = leftFighter;
            rightFighter.allowCombo = false;
            setTimeout(() => {
              rightFighter.allowCombo = true;
            }, 10000);
            break;

          default:
            alert('Неизвестное значение');
        }
        // console.log(leftFighter);
        // console.log(rightFighter);

        //calculate action
        if (attacker && defender)
          switch (key) {
            case 'PlayerOneAttack':
            case 'PlayerTwoAttack':
            case 'PlayerOneCriticalHitCombination':
            case 'PlayerTwoCriticalHitCombination':
              //Get Damage
                damage = getDamage(attacker, defender);
              //Calculate HP
              defender.dynamicHealth -= damage;
              if (defender.dynamicHealth <= 0) {
                Winner(attacker);
                defender.dynamicHealth = 0;
              }
              //Display HP
              defender.hpWidth = defender.dynamicHealth / defender.health * 100;
              defender.healthIndicator.style.width = `${defender.hpWidth}%`;
              defender.healthIndicator.innerHTML = `${defender.dynamicHealth}hp`;
          }



        console.log({
          key,
          'damage': damage,
        });
        console.log({
          'after': 'after',
          'first health': leftFighter.dynamicHealth,
          'second health': rightFighter.dynamicHealth,

        });
        console.log(' ');

      }
    }

  });

  document.addEventListener('keyup', (event) => {
    const keyName = event.code;
    if (controls['PlayerOneCriticalHitCombination'].includes(keyName)) {

      leftFighter.combo = leftFighter.combo.filter(keyCode => keyCode != keyName)

    }

    if (controls['PlayerTwoCriticalHitCombination'].includes(keyName)) {
      rightFighter.combo = rightFighter.combo.filter(keyCode => keyCode != keyName)
    }


    for (const [key, value] of Object.entries(controls)) {

      if (keyName == value) {
        switch (key) {
          case 'PlayerOneAttack':
            leftFighter.isAttacking = false;
            break;
          case 'PlayerOneBlock':
            leftFighter.isDefending = false;
            break;
          case 'PlayerTwoAttack':
            rightFighter.isAttacking = false;
            break;
          case 'PlayerTwoBlock':
            rightFighter.isDefending = false;
            break;
          default:
            alert('Неизвестное значение');
        }
      }
    }

  });
  const Winner = (winner) => {

    return new Promise((resolve, reject) => {
      // resolve the promise with the winner when fight is over
      if (winner == rightFighter) {
        rightFighter.rightFighterWin = true;
      } else if (winner == leftFighter) {
        leftFighter.leftFighterWin = true;
      }
      if (winner) {
        resolve(showWinnerModal(winner))
      } else {
        reject(console.log("Error."))
      }


    });
  }
}
export function getDamage(attacker, defender) {
  // return damage 

  // console.log(attacker, defender)
  let damage = getHitPower(attacker) - (defender.isDefending && !attacker.isCombo ? getBlockPower(defender) : 0);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  let max = 2;
  const criticalHitChance = Math.ceil(Math.random() * (max));
  let hitpower;
  hitpower = attack * criticalHitChance
  if (fighter.isCombo) {
    hitpower = attack * 2;
  }
  return hitpower
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  let max = 2;
  const dodgeChance = Math.ceil(Math.random() * (max));
  let blockpower = defense * dodgeChance;

  return blockpower
}
