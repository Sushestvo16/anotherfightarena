import { callApi } from '../helpers/apiHelper';
import { createFighterPreview } from '../components/fighterPreview'

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method

    // endpoint - `details/fighter/${id}.json`;

    let apiResult;
    try {
      const endpoint = `details/fighter/${id}.json`;
      apiResult = await callApi(endpoint, 'GET');

      return apiResult;

    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
